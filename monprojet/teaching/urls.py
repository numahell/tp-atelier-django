from django.urls import path
from teaching import views

urlpatterns = [
    path('coursematerial', views.CourseMaterialList.as_view(), name='coursematerial-list'),
    path('coursematerial/<int:pk>', views.CourseMaterialDetail.as_view(), name='coursematerial-detail'),
    path('coursematerial/add', views.CourseMaterialCreateView.as_view(), name='coursematerial-create'),
]
