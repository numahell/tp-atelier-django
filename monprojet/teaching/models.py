from django.db import models


class Author(models.Model):
    """Model for Author"""

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)


class CourseMaterial(models.Model):
    """Model for CourseMaterial"""

    title = models.CharField(max_length=100)
    description = models.TextField(
        max_length=500, blank=True, null=True
    )
    author = models.ForeignKey(
        'Author', related_name='author',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    url = models.URLField(
        max_length=200, blank=True, null=True
    )

    def __str__(self):
        return self.title
