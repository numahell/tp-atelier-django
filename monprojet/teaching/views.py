from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView

from teaching.forms import CourseMaterialForm
from teaching.models import CourseMaterial

def home_view(request):
    return HttpResponse("<h1>Bienvenue !</h1>")


class CourseMaterialList(ListView):
    """CourseMaterial list view"""
    model = CourseMaterial


class CourseMaterialDetail(DetailView):
    """CourseMaterial detail view"""
    model = CourseMaterial


class CourseMaterialCreateView(CreateView):
    """CourseMaterial create view"""
    form_class = CourseMaterialForm
    template_name = 'teaching/coursematerial_form.html'
    success_url = '/teaching/coursematerial'
